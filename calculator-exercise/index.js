const read = require('readline-sync')

const calculator = () => {
    const num1 = Number(read.question('Enter a number'))

    const operators = ['+', '-', '/', '*']
    operators.forEach((op, i) => {
        console.log(`[${i + 1}] ${op}`)
    })
    const selectedOp = read.question('Choose and operator')
    const num2 = Number(read.question('Enter another number'))

    var operatorText = operators.find((op, i) => {
        return i + 1 == selectedOp
    })
    console.log(`${num1} ${operatorText} ${num2} = ${calculate(num1, num2, selectedOp)}`)
    
    const answer = read.question('Restart?')
    if (answer !== 'y' && answer != 'Y')
        process.exit()

    console.clear()
    console.log('Restarted')
    calculator()
}

const calculate = (num1, num2, op) => {
    if (op == 1)
        return num1 + num2
    else if (op == 2)
        return num1 - num2
    else if (op == 3)
        return num1 / num2
    else if (op == 4)
        return num1 * num2
}


calculator()