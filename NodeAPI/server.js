var express = require('express')
var bodyParser = require('body-parser')
var app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

var people = [
    { name: 'Mayer', id: 1 },
    { name: 'Jason', id: 2 },
    { name: 'Meir', id: 3 },
    { name: 'Qizard', id: 4 },
    { name: 'Morris', id: 5 }
]

app.get('/', function (req, res) {
    res.status(200).send(people)
})

app.post('/add', function (req, res) {
    var person = req.body
    if (!person.name)
        res.status(500).send({ error: 'You must have a person name in order to add' })
    else {
        people.push(person)
        res.status(200).send(person)
    }
})

app.put('/person/:personid', function (req, res) {
    var personid = req.params.personid
    var name = req.body.name

    if (!name)
        res.status(500).send({ error: 'You must pass a name to change one' })
    else {

        var pid = people.findIndex(function (p) {
            return p.id == personid
        })

        if (!pid && pid !== 0)
            res.status(500).send({ error: 'Invalid personid' })
        else {
            people[pid].name = name
            res.status(200).send(people[pid])
        }
    }
})

app.listen(3001, function () {
    console.log('Hey my API is listening on port 3001!!')
})


//Using readline-sync (You need NPM For this).
//Ask the user for a number
//Ask the user to choose an operator.
//Ask the user for a second number
//Output the total for the users equation

