const express = require('express')
const app = express()

const routes = require('./routes')

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
})

app.use('/', routes)

app.listen('3001', () => {
    console.log('Office Shop running on port 3001')
})

