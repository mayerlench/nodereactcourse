const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const router = express.Router()
const mongoose = require('mongoose')
const db = mongoose.connect('mongodb://localhost/office_shop')

const Product = require('../models/product')
const WishList = require('../models/wishList')

router.use(bodyParser.json())
router.use(bodyParser.urlencoded({ extended: false }))

router.get('/product', function (req, res) {
    Product.find({}, function (err, products) {
        if (err) res.status(500).send({ error: "Could not get product" })
        else res.send(products)
    })
})

router.post('/product', (req, res) => {
    var product = new Product(req.body)

    product.save((err, savedProduct) => {
        if (err)
            res.status(500).send({ error: "Could not save product" })

        else
            res.send(savedProduct)
    })
})

router.get('/wishlist', (req, res) => {
    WishList.find({})
    .populate({ path: 'products', model: 'Product' })
    .exec((err, wishLists) => {
        if (err)
            res.status(500).send({ error: 'Could not populate products' })
        else
            res.send(wishLists)
    })
})

router.post('/wishlist', (req, res) => {
    var wishList = new WishList(req.body)

    wishList.save((err, savedWishList) => {
        if (err)
            res.status(500).send({ error: "Could not save wishlist" })
        else
            res.send(savedWishList)
    })
})

router.put('/wishlist/products/add', (req, res) => {
    Product.findOne({ _id: req.body.productId }, (err, product) => {
        if (err) return res.status(500).send({ error: "Could not find product" })

        else {
            WishList.update({ _id: req.body.wishlistId }, {
                $addToSet: { products: product._id }
            }, function (err, wishlist) {
                if (err)
                    res.status(500).send({ error: "Could not add item to wishlist" })
                else {
                    res.send(wishlist)
                }
            })
        }
    })
})


module.exports = router