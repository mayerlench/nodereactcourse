const mongoose = require('mongoose')
const Schema = mongoose.Schema

const product = new Schema({
    title: { type: String, required: true },
    price: { type: Number, required: true },
    imgUrl: { type: String, default: 'http://via.placeholder.com/600x800' },
    description: { type: String, default: 'This item has no description' }
})

module.exports = mongoose.model('Product', product)