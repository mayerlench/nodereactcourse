import React, { Component } from 'react';
import logo from './logo.svg';
import './app.css';

//Services
import http from '../services/http-service'

//Components
import Product from '../components/product/product.js'
import WishList from '../components/wishlist/wishlist.js'

class App extends Component {

  constructor(props) {
    super(props)

    this.state = { products: [] }

    this.loadData()
  }

  loadData = () => {
    const self = this
    http.getProducts().then(products => {
      self.setState({ products: products })
    }, err => {

    })
  }

  productList = () => {
    const list = this.state.products.map(product =>
      <div className="col-sm-4" key={product._id} >
        <Product id={product._id}  price={product.price} title={product.title} imgUrl={product.imgUrl}
          description={product.description} />
      </div>
    )

    return list
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to the Office Shop</h1>
        </header>

        <div className="container-fluid App-main">
          <div className="row">
            <div className="col-sm-8">
              <div className="row">
                {this.productList()}
              </div>
            </div>


            <div className="col-sm-4">
              <WishList />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
