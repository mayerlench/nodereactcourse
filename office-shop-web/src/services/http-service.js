import 'whatwg-fetch'

const api = 'http://localhost:3001'

const resolveProm = (promise) => {
    return new Promise((resolve, reject) => {
        promise.then(response => {
            resolve(response.json())
        })
    })
}

const getProducts = () => {
    return resolveProm(fetch(`${api}/product`))
}

export default {
    getProducts: getProducts
}