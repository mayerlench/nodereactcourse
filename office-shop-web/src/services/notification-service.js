import { concat, findIndex, remove, propEq } from 'ramda'

export const NOTIF_WL_CHANGED = "notif_wishlist_changed"

let observers = {}

const addObserver = (notifName, observer, callback) => {
    let obs = observers[notifName]

    if (!obs)
        obs = []

    observers[notifName] = concat(obs, [{ observer: observer, callback: callback }])
}

const removeObserver = (notifName, observer) => {
    var obs = observer[notifName]
    if (obs) {
        var index = findIndex(propEq('observer', observer))(obs)
        observers[notifName] = remove(index, 1)(obs)
    }
}

const postNotification = (notifName, data) => {
    let obs = observers[notifName]
    obs.forEach(ob => {
        ob.callback(data)
    })
}

export default {
    postNotification: postNotification,
    removeObserver: removeObserver,
    addObserver: addObserver
}