import { find, findIndex, remove, propEq } from 'ramda'

import NS, { NOTIF_WL_CHANGED } from './notification-service'

var wishlist = []

const addWishlistItem = item => {
    wishlist.push(item)
    NS.postNotification(NOTIF_WL_CHANGED, wishlist)
}

const removeWishlistItem = item => {
    var index = findIndex(propEq('id', item.id))(wishlist)

    if (index > -1) {
        wishlist = remove(index, 1)(wishlist)
        NS.postNotification(NOTIF_WL_CHANGED, wishlist)
    }
}

const itemOnWishlist = item => {
    return find((wl) => {
        return wl.id === item.id
    }, wishlist) ? true : false
}

export default {
    removeWishlistItem: removeWishlistItem,
    itemOnWishlist: itemOnWishlist,
    addWishlistItem: addWishlistItem
}