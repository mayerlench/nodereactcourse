import React, { Component } from 'react'
import './product.css'

import DS from '../../services/data-service'
import NS, { NOTIF_WL_CHANGED } from '../../services/notification-service';

class Product extends Component {

    constructor(props) {
        super(props)
        this.state = { onWishlist: DS.itemOnWishlist() }
    }

    componentDidMount = () => {
        NS.addObserver(NOTIF_WL_CHANGED, this, this.onWishlistChanged)
    }

    componentWillMount = () => {
        NS.removeObserver(this, NOTIF_WL_CHANGED)
    }

    onWishlistChanged = () => {
        this.setState({ onWishlist: DS.itemOnWishlist(this.props) })
    }

    onBtnClicked = () => {
        if (this.state.onWishlist) 
            DS.removeWishlistItem(this.props)
        else
            DS.addWishlistItem(this.props)
    }

    render() {
        var btnClass = this.state.onWishlist ? "btn btn-danger" : "btn btn-primary"
        var btnText = this.state.onWishlist ? "Remove From WishList" : "Add To WishList"

        return (
            <div className="card product">
                <img className="card-img-top" src={this.props.imgUrl} alt="Product"></img>
                <div className="card-block">
                    <h4 className="card-title">{this.props.title}</h4>
                    <p className="text-muted">{this.props.description}</p>
                    <p className="card-text">Price ${this.props.price}</p>
                    <a href="#/" onClick={this.onBtnClicked} className={btnClass}>{btnText}</a>
                </div>
            </div>
        )
    }
}

export default Product