import React, { Component } from 'react'
import './product-condensed.css'

import NS, { NOTIF_WL_CHANGED } from '../../services/notification-service'
import DS from '../../services/data-service'

class ProductCondensed extends Component {
    constructor(props) {
        super(props)
        this.state = { wishlist: [] }
    }

    componentDidMount = () => {
        console.log('product componentDidMount')
        NS.addObserver(NOTIF_WL_CHANGED, this, () => {})
    }

    componentWillMount = () => {
        console.log('product componentWillMount')
        NS.removeObserver(this, NOTIF_WL_CHANGED)
    }

    onBtnClicked = () => {
        let product = this.props.product
        if (DS.itemOnWishlist(product))
            DS.removeWishlistItem(product)
        else
            DS.addWishlistItem(product)
    }

    render() {
        const { title, price } = this.props.product
        return (
            <li className="list-group-item pc-condensed">
                <a onClick={this.onBtnClicked} className="btn btn-outline-danger">X</a>
                <p>{title} | <b>${price}</b></p>
            </li>
        )
    }
}

export default ProductCondensed