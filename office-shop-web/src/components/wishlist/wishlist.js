import React, { Component } from 'react'
import './wishlist.css'

import ProductCondensed from '../product-condensed/product-condensed'

import NS, { NOTIF_WL_CHANGED } from '../../services/notification-service'
import DS from '../../services/data-service'

class Wishlist extends Component {
    constructor(props) {
        super(props)

        this.state = {
            wishlist: []
        }
    }

    componentDidMount = () => {
        NS.addObserver(NOTIF_WL_CHANGED, this, this.onWishlistChanged)
    }

    componentWillMount = () => {
        NS.removeObserver(this, NOTIF_WL_CHANGED)
    }

    onWishlistChanged = (newWishlist) => {
        this.setState({ wishlist: newWishlist })
    }


    createWishlist = () => {
        return this.state.wishlist.map(product =>
            <ProductCondensed product={product} key={product.id}/>
        )
    }

    render() {
        return (
            <div className="card">
                <div className="card-block">
                    <h4 className="card-title">Wishlist</h4>
                    <ul className="list-group">
                        {this.createWishlist()}
                    </ul>
                </div>
            </div>
        )
    }
}

export default Wishlist